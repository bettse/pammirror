// read_mir-ror.c
/*
 * Copyright (c) 2009 - Jo�l PASTRE 'Jopa'  <joel@jopa.fr> - http://www.jopa.fr
 *
 * This file is part of the pam_mir-ror project. pam_mir-ror is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * pam_mir-ror is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>

#include "mir-ror.h"

int main() {
   char device[50];
   char tag[25];
   
   if (detect_mirror(device)!=0) {
      printf ("Unable to detect Mir:ror device...\n");
      return(-1);
   }

   if (get_mirror_tag(tag,device)!=0) {
     printf ("Failed to read Mir:ror device %s...\n",device);
     return(-1);
   }
    printf("%s\n",tag);
    return(0); 
}
