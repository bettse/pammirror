// mir-ror.c
/*
 * Copyright (c) 2009 - Jo�l PASTRE 'Jopa'  <joel@jopa.fr> - http://www.jopa.fr
 *
 * This file is part of the pam_mir-ror project. pam_mir-ror is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * pam_mir-ror is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <asm/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/hiddev.h>
#include <linux/input.h>
#include <string.h>
#include <glob.h>
#include <pwd.h>

#include <unistd.h>

#define HIDIOCGRAWINFO          _IOR('H', 0x03, struct hidraw_devinfo)


struct hidraw_devinfo {
        __u32 bustype;
         __s16 vendor;
         __s16 product;
};


#include "mir-ror.h"

//----------------------------------------------------------------------------
// glob_devices ( extract from discover_device.c of Mirware pack)

int glob_devices(glob_t * globbuf) {
	int ret;
	ret = glob("/dev/hidraw*", 0, NULL, globbuf);
	if(ret != 0) {
			printf("DEBUG: glob() failed.\n");
	}
	return ret;
}

//----------------------------------------------------------------------------
// detect_mirror ( adapted main() function of the discover_device program 
// from Mirware pack

int detect_mirror(char * device) {
	int fd;
	struct hidraw_devinfo hrdi;
	glob_t globbuf;
	unsigned int i;
	int exit_value = -1;
	
	
	glob_devices(&globbuf);
	for(i=0;i<globbuf.gl_pathc;i++) {
		if((fd=open(globbuf.gl_pathv[i], O_RDONLY)) < 0) {
			printf("error while opening %s.\n", globbuf.gl_pathv[i]);
			continue;
		}
		ioctl(fd,HIDIOCGRAWINFO,&hrdi);
		if(hrdi.vendor == 0x1da8 && hrdi.product == 0x1301) {
			//printf("Found device: %s\n", globbuf.gl_pathv[i]);
			//chmod(globbuf.gl_pathv[i], S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
			//fwrite(globbuf.gl_pathv[i], strlen(globbuf.gl_pathv[i]),1, fh);
			sprintf(device,"%s",globbuf.gl_pathv[i]);
			exit_value = 0;
		}
		close(fd);
	}

	return exit_value;
}

//----------------------------------------------------------------------------
// Get_mirror_tag : Access to reader and wait for 0x102 event. Return the Id
//                  of an approchead tag.

int get_mirror_tag(char * tag, char * device) {
  
  FILE *mirror;
  short unsigned int block;
  short unsigned int event;
  int i;

  // Open device
  mirror = fopen(device,"rb");
  if (!mirror) {
     printf("Error while opening %s Mir:ror device.\n",device);
     return -1;
  }
  // Read Loop
  while (event!=0x102) {
     fread(&event,2,1,mirror);
  }
  //RFID Tag Detected  (0x102 event) - Reading rfid 
  for (i=0;i<6;i++) {
	fread(&block,2,1,mirror);
        sprintf(&tag[i*4],"%04X",block);
  }
  return 0;
} // get_mirror_tag


//-----------------------------------------------------------------------------
// get_user_tagfile : Build ~/.auth path using user name and passwd file.
// 		      return filename with complete path.

void get_user_tagfile(char *user, char *tagfile) {
  struct passwd *pw;
  pw = getpwnam (user);
  sprintf(tagfile,"%s/%s",pw->pw_dir,TAGFILE);
}
