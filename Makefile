# Makefile pam_mir-ror

CC = gcc
CFLAGS += -Wall -fPIC
LDFLAGS += -shared -lpam -lm -lpthread

OBJS1=mir-ror.o pam_mir-ror.o
OBJS2=mir-ror.o read_mir-ror.o

OBJS=mir-ror.o pam_mir-ror.o read_mir-ror.o

all: pam_mir-ror read_mir-ror

pam_mir-ror: $(OBJS1)
	$(CC) $(OBJS1) $(CFLAGS) $(LDFLAGS) -o pam_mir-ror.so

read_mir-ror: $(OBJS2)
	$(CC) $(OBJS2) -o read_mir-ror

clean: 
	rm -f $(OBJS1) $(OBJS2) pam_mir-ror.so read_mir-ror
