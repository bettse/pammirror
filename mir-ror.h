//mir-ror.h 
/*
 * Copyright (c) 2009 - Jo�l PASTRE 'Jopa'  <joel@jopa.fr> - http://www.jopa.fr
 *
 * This file is part of the pam_mir-ror project. pam_mir-ror is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * pam_mir-ror is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */
#define TAGFILE	".authtag"

//mir:ror device detection.
//Inspired from part of Mirware : discover-device.c 
int detect_mirror(char *device);

//Read from mir:ror device and return tag id.
int get_mirror_tag(char *tag, char *device);

//Build RFID file path from user name and passwd file.
void get_user_tagfile(char *user, char *tagfile);

